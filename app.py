from flask import Flask, request, session, redirect, render_template
import sqlite3


app = Flask(__name__)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'
conn = sqlite3.connect('/usr/data/data.db', check_same_thread=False)
c = conn.cursor()


@app.route('/')
def index():
    if 'username' in session:
        return redirect('/todos')
    return render_template('index.html')


@app.route('/signup', methods=['GET', 'POST'])
def signup():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']

        if get_existing_user(username):
            return 'User with that username already exists'

        c.execute(
            'insert into users (username, password) values (?, ?)',
            (username, password)
        )
        conn.commit()
        session['username'] = username
        return redirect('/todos')
    return render_template('signup.html')


@app.route('/signin', methods=['GET', 'POST'])
def signin():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        existing_user = get_existing_user(username)

        if not existing_user:
            return 'Username or password is wrong'

        saved_password = existing_user[2]
        if saved_password != password:
            return 'Username or password is wrong'

        session['username'] = username
        return redirect('/todos')

    return render_template('signin.html')


@app.route('/todos', methods=['GET', 'POST', 'PUT'])
def list_todos():
    if 'username' not in session:
        return redirect('/login')

    user = get_existing_user(session['username'])
    user_id = user[0]

    if request.method == 'POST':
        todo_text = request.form['todo_text']
        c.execute(
            'insert into todos (todo_text, todo_state, user_id) values (?, ?, ?)',
            (todo_text, 'TODO', user_id)
        )
        conn.commit()
        return redirect('/todos')

    if request.method == 'GET':
        todos = get_todos(user_id)
        done_todos = get_done(user_id)
        return render_template('todos.html',
                               todos=todos, done_todos=done_todos)


@app.route('/todos/mark-done', methods=['POST'])
def mark_done():
    todo_id = request.form['id']
    print(todo_id)
    c.execute('update todos set todo_state="DONE" where id=?', (todo_id,))
    conn.commit()
    return redirect('/todos')


@app.route('/signout', methods=['GET'])
def signout():
    if 'username' in session:
        session.pop('username', None)

    return redirect('/')


def get_done(user_id):
    done = c.execute(
        'select * from todos where user_id=? and todo_state="DONE"',
        (user_id,)
    ).fetchall()
    return done


def get_existing_user(username):
    user = c.execute(
        'select * from users where username=?',
        (username,)).fetchone()
    return user


def get_todos(user_id):
    todos = c.execute(
        'select * from todos where user_id=? AND todo_state="TODO"',
        (user_id,)).fetchall()
    return todos


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)
