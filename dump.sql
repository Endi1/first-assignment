create table if not exists users (
       id integer primary key,
       username text,
       password text
);

create table if not exists todos (
       id integer primary key,
       todo_text text,
       todo_state text,
       user_id integer,

       foreign key (user_id) references users(id)
);
