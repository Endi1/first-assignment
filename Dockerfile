FROM python:3

WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app
RUN pip install --no-cache-dir -r requirements.txt

RUN apt-get update
RUN apt-get install -y sqlite3
RUN mkdir -p /usr/data

COPY . /usr/src/app
RUN sqlite3 /usr/data/data.db < dump.sql

CMD ["python", "/usr/src/app/app.py"]